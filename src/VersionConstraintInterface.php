<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

use Stringable;

/**
 * VersionConstraintInterface interface file.
 * 
 * A Version Constraint consists on a version and an operator that defines a
 * domain in version space, in which versions can be reached or not. Such
 * version constraints can represent a prism in version space, or can be split
 * into smaller parts due to the unions and intersections of version
 * constraints.
 * 
 * Version Constraints are considered immutable; all methods that might change
 * state MUST be implemented such that they retain the internal state of the
 * current version and return an instance that contains the changed state.
 * 
 * @author Anastaszor
 */
interface VersionConstraintInterface extends Stringable
{
	
	/**
	 * The operator of the version constraint.
	 * 
	 * @return VersionOperatorInterface
	 */
	public function getOperator() : VersionOperatorInterface;
	
	/**
	 * Gets the range this version constraint defines with its base version
	 * and its operator.
	 * 
	 * @return VersionRangeInterface
	 */
	public function getRange() : VersionRangeInterface;
	
	/**
	 * Gets whether this version constraint equals the other object. For two
	 * version constraints to be equal, they must define the same version space.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
}
