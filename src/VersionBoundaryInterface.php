<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

use Stringable;

/**
 * VersionBoundaryInterface interface file.
 * 
 * A Version Boundary consists on a boundary that is represented by a version
 * and whether this version is inside the boundary.
 * 
 * Version Boundaries are considered immutable; all methods that might change
 * state MUST be implemented such that they retain the internal state of the
 * current version and return an instance that contains the changed state.
 * 
 * @author Anastaszor
 */
interface VersionBoundaryInterface extends Stringable
{
	
	/**
	 * Gets the version number that defines this boundary.
	 * 
	 * @return VersionInterface
	 */
	public function getBaseVersion() : VersionInterface;
	
	/**
	 * Gets whether the version that defines this boundary must be considered
	 * included within the half segment that this boundary borders.
	 * 
	 * @return boolean
	 */
	public function isIncluded() : bool;
	
	/**
	 * Gets whether this version boundary equals the other object. For two
	 * version boundaries to be equal, they must be defined by the same version
	 * number and inclusions status.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Returns the version boundary that is the lowest between this boundary
	 * and the other boundary. If both boundaries have the same limit, the one
	 * with the included limit is the min (this assumes that this boundary and
	 * the other boundary are minimum ranges respectively).
	 * 
	 * @param ?VersionBoundaryInterface $other
	 * @return VersionBoundaryInterface
	 */
	public function minMin(?VersionBoundaryInterface $other) : VersionBoundaryInterface;
	
	/**
	 * Returns the version boundary that is the lowest between this boundary
	 * and the other boundary. If both boundaries have the same limit, the one
	 * with the excluded limit is the min (this assumes that this boundary and
	 * the other boundary are maximum ranges respectively).
	 * 
	 * @param ?VersionBoundaryInterface $other
	 * @return VersionBoundaryInterface
	 */
	public function minMax(?VersionBoundaryInterface $other) : VersionBoundaryInterface;
	
	/**
	 * Returns the version boundary that is the highest between this boundary
	 * and the other boundary. If both boundaries have the same limit, the one
	 * with the excluded limit is the max (this assumes that this boundary and
	 * the other boundary are minimum ranges respectively). If null is provided
	 * (meaning there is no limit), then it is also returned.
	 * 
	 * @param ?VersionBoundaryInterface $other
	 * @return ?VersionBoundaryInterface
	 */
	public function maxMin(?VersionBoundaryInterface $other) : ?VersionBoundaryInterface;
	
	/**
	 * Returns the version boundary that is the highest between this boundary
	 * and the other boundary. If both boundaries have the same imit, the one
	 * with the included limit is the max (this assumes that this boundary and
	 * the other boundary are maximum ranges respectively). If null is provided
	 * (meaning there is no limit), then it is also returned.
	 * 
	 * @param ?VersionBoundaryInterface $other
	 * @return ?VersionBoundaryInterface
	 */
	public function maxMax(?VersionBoundaryInterface $other) : ?VersionBoundaryInterface;
	
}
