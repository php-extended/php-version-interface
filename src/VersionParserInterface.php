<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

use PhpExtended\Parser\ParserInterface;

/**
 * VersionParserInterface interface file.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<VersionInterface>
 */
interface VersionParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
