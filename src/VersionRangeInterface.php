<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

use Stringable;

/**
 * VersionRangeInterface interface file.
 * 
 * A Version Range is the instanciation of one or many version constraints, that
 * defines explicit boundaries. Such version range can represent a prism in
 * version space, or can be split into smaller parts due to the unions and
 * intersections of version ranges. As such, the segment defined by the lower
 * and upper bounds can contains more elements than elements of this range.
 * 
 * Version Ranges are considered immutable; all methods that might change
 * state MUST be implemented such that they retain the internal state of the
 * current version and return an instance that contains the changed state.
 * 
 * @author Anastaszor
 */
interface VersionRangeInterface extends Stringable
{
	
	/**
	 * Gets the list of all the intervals that compose this range (which
	 * represent this full range once unioned together).
	 * 
	 * @return array<integer, VersionSegmentInterface>
	 */
	public function getSegments() : array;
	
	/**
	 * Gets whether this version range cannot contains any verstion number.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets whether this version range equals the other object. For two version
	 * ranges to be equal, the version space they define must be equal.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Gets whether the given version number is included within this version
	 * range.
	 * 
	 * @param VersionInterface $version
	 * @return boolean
	 */
	public function containsVersion(VersionInterface $version) : bool;
	
	/**
	 * Gets whether the given version boundary is included within this version
	 * range.
	 * 
	 * @param ?VersionBoundaryInterface $boundary
	 * @return boolean
	 */
	public function containsBoundary(?VersionBoundaryInterface $boundary) : bool;
	
	/**
	 * Gets whether the given version interval is included within this version
	 * range.
	 *
	 * @param VersionSegmentInterface $interval
	 * @return boolean
	 */
	public function containsSegment(VersionSegmentInterface $interval) : bool;
	
	/**
	 * Gets whether the given version range is included within this version
	 * range, meaning all the versions in the argument range are included in
	 * this version range.
	 * 
	 * @param VersionRangeInterface $range
	 * @return boolean
	 */
	public function containsRange(VersionRangeInterface $range) : bool;
	
	/**
	 * Creates a new range that is the union of this range and the given other
	 * range, meaning it contains all the versions that are present in at least
	 * one of the ranges.
	 * 
	 * @param VersionRangeInterface $range
	 * @return VersionRangeInterface
	 */
	public function union(VersionRangeInterface $range) : VersionRangeInterface;
	
	/**
	 * Creates a new range that is the intersection of this range and the given
	 * other range, meaning it contains all the versions that are present in
	 * both of the ranges.
	 * 
	 * @param VersionRangeInterface $range
	 * @return VersionRangeInterface
	 */
	public function intersect(VersionRangeInterface $range) : VersionRangeInterface;
	
	/**
	 * Creates a new range that is the subtraction of this range and the given
	 * other range, meaning it contains all the versions that are present in
	 * this set but not in the other range.
	 * 
	 * @param VersionRangeInterface $range
	 * @return VersionRangeInterface
	 */
	public function subtract(VersionRangeInterface $range) : VersionRangeInterface;
	
}
