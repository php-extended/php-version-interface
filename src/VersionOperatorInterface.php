<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

use Stringable;

/**
 * VersionOperatorInterface interface file.
 * 
 * A Version Operator is a definer of a domain that may apply to any version
 * number that gives a certain
 * This interface represents an operator which, applied to a given version
 * number defines a range of acceptable versions.
 * 
 * Version Operators are considered immutable; all methods that might change
 * state MUST be implemented such that they retain the internal state of the
 * current version and return an instance that contains the changed state.
 * 
 * @author Anastaszor
 */
interface VersionOperatorInterface extends Stringable
{
	
	/**
	 * Gets the range that is given by this operator applied to the given base
	 * version number.
	 * 
	 * @param VersionInterface $base
	 * @return VersionRangeInterface
	 */
	public function getRange(VersionInterface $base) : VersionRangeInterface;
	
	/**
	 * Gets whether this version operator equals the other object. For two
	 * operators to be equal, they must define the same version space for any
	 * given version number.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
}
