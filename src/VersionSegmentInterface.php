<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

use Stringable;

/**
 * VersionSegmentInterface interface file.
 * 
 * A Version Segment is the instanciation of an inferior boundary and a
 * superior boundary. A version interval is a continuous segments of versions
 * that cannot be changed.
 * 
 * Version Segments are considered immutable; all methods that might change
 * state MUST be implemented such that they retain the internal state of the
 * current version and return an instance that contains the changed state.
 * 
 * @author Anastaszor
 */
interface VersionSegmentInterface extends Stringable
{
	
	/**
	 * Gets the version boundary that represents the lower bound of this
	 * segment. Such boundary can always be created, because the lower version
	 * 0.0.0 represents the zero in terms of version space, and version number
	 * cannot be negative.
	 *
	 * @return VersionBoundaryInterface
	 */
	public function getLowerBound() : VersionBoundaryInterface;
	
	/**
	 * Gets the version boundary that represents the upper bound of this
	 * segment. If no such boundary can be created (meaning the operator is
	 * boundless), then null is returned.
	 *
	 * @return ?VersionBoundaryInterface
	 */
	public function getUpperBound() : ?VersionBoundaryInterface;
	
	/**
	 * Gets whether this version segment cannot contains any verstion number.
	 *
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets whether this version segment equals the other object. For two
	 * version segments to be equal, the version space they define must be
	 * equal.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Gets whether the given version number is included within this version
	 * interval.
	 *
	 * @param VersionInterface $version
	 * @return boolean
	 */
	public function containsVersion(VersionInterface $version) : bool;
	
	/**
	 * Gets whether the given version boundary is included within this version
	 * interval.
	 *
	 * @param ?VersionBoundaryInterface $boundary
	 * @return boolean
	 */
	public function containsBoundary(?VersionBoundaryInterface $boundary) : bool;
	
	/**
	 * Gets whether the given version interval is included within this version
	 * interval.
	 * 
	 * @param VersionSegmentInterface $interval
	 * @return boolean
	 */
	public function containsSegment(VersionSegmentInterface $interval) : bool;
	
	/**
	 * Creates a new range that is the union of this segment and the given
	 * other segment, meaning it contains all the versions that are present in
	 * at least one of the segments.
	 *
	 * @param VersionSegmentInterface $interval
	 * @return VersionRangeInterface
	 */
	public function union(VersionSegmentInterface $interval) : VersionRangeInterface;
	
	/**
	 * Creates a new range that is the intersection of this segment and the
	 * given other segment, meaning it contains all the versions that are
	 * present in both of the segments.
	 *
	 * @param VersionSegmentInterface $interval
	 * @return VersionRangeInterface
	 */
	public function intersect(VersionSegmentInterface $interval) : VersionRangeInterface;
	
	/**
	 * Creates a new range that is the subtraction of this segment and the
	 * given other segment, meaning it contains all the versions that are
	 * present in this set but not in the other segment.
	 *
	 * @param VersionSegmentInterface $interval
	 * @return VersionRangeInterface
	 */
	public function subtract(VersionSegmentInterface $interval) : VersionRangeInterface;
	
}
