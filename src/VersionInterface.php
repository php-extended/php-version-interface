<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

use Stringable;

/**
 * VersionInterface interface file.
 * 
 * A Version consists of collection of ordered numbers that must increment as
 * development completes. This interface defines the methods for serializing
 * and unserialazing version numbers and compare them.
 * 
 * Version numbers are considered immutable; all methods that might change
 * state MUST be implemented such that they retain the internal state of the
 * current version and return an instance that contains the changed state.
 * 
 * @author Anastaszor
 */
interface VersionInterface extends Stringable
{
	
	/**
	 * Gets an integer conversion for this version number. Major version number
	 * is multiplied by 10,000, minor version by 100, and patch version by 1.
	 * In this representation, the label is ignored.
	 * 
	 * @return integer
	 */
	public function toInteger() : int;
	
	/**
	 * Gets a string array with all the version parts.
	 * 
	 * @return array<integer, string>
	 */
	public function toArray() : array;
	
	/**
	 * Gets the major version number for this version.
	 * 
	 * @return integer
	 */
	public function getMajor() : int;
	
	/**
	 * Gets the minor version number for this version.
	 * 
	 * @return integer
	 */
	public function getMinor() : int;
	
	/**
	 * Gets the patch version number for this version.
	 * 
	 * @return integer
	 */
	public function getPatch() : int;
	
	/**
	 * Gets the label for this version. String that begin with (in decreasing
	 * order) "RC", "alpha", "beta" may be recognised for orderings, other
	 * strings will be compared alphabetically.
	 * 
	 * @return string
	 */
	public function getLabel() : string;
	
	/**
	 * Gets whether this version is a development version.
	 * 
	 * @return boolean
	 */
	public function isDeveloppement() : bool;
	
	/**
	 * Returns an instance with the major version incremented by 1.
	 * 
	 * @return VersionInterface
	 */
	public function incrementMajor() : VersionInterface;
	
	/**
	 * Returns an instance with the minor version incremented by 1.
	 * 
	 * @return VersionInterface
	 */
	public function incrementMinor() : VersionInterface;
	
	/**
	 * Returns an instance with the patch version incremented by 1.
	 * 
	 * @return VersionInterface
	 */
	public function incrementPatch() : VersionInterface;
	
	/**
	 * Returns an instance with the specified label.
	 * 
	 * @param string $label
	 * @return VersionInterface
	 */
	public function setLabel(string $label) : VersionInterface;
	
	/**
	 * Gets whether this version equals the other object. For two versions to
	 * be equal, their major, minor, patch and labels must be equal.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Returns the version that is the lowest between this version and the
	 * other version.
	 * 
	 * @param VersionInterface $other
	 * @return VersionInterface
	 */
	public function min(VersionInterface $other) : VersionInterface;
	
	/**
	 * Returns the version that is the highest between this version and the
	 * other version.
	 * 
	 * @param VersionInterface $other
	 * @return VersionInterface
	 */
	public function max(VersionInterface $other) : VersionInterface;
	
	/**
	 * Gets whether this VersionInterface is strictly greater than the other
	 * VersionInterface.
	 * 
	 * @param VersionInterface $other
	 * @return boolean
	 */
	public function isStrictlyGreaterThan(VersionInterface $other) : bool;
	
	/**
	 * Gets whether this VersionInterface is greater than or equals the other
	 * VersionInterface.
	 * 
	 * @param VersionInterface $other
	 * @return boolean
	 */
	public function isGreaterThanOrEquals(VersionInterface $other) : bool;
	
	/**
	 * Gets whether this VersionInterface is strictly lower than the other
	 * VersionInterface.
	 * 
	 * @param VersionInterface $other
	 * @return boolean
	 */
	public function isStrictlyLowerThan(VersionInterface $other) : bool;
	
	/**
	 * Gets whether this VersionInterface is lower than or equals the other
	 * VersionInterface.
	 * 
	 * @param VersionInterface $other
	 * @return boolean
	 */
	public function isLowerThanOrEquals(VersionInterface $other) : bool;
	
}
